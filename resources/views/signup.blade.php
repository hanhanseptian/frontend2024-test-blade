<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign Up</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    <style>
        body {
            height: 100vh;
            width: 100vw;
            font-family: 'Nunito', sans-serif;
            display: flex;
            align-items: center;
            justify-content: center;
            justify-items: center;
            background: linear-gradient(90deg, rgb(25, 21, 95) 0%, rgb(65, 65, 156) 35%, rgb(76, 215, 243) 100%);
        }

        .login-container {
            width: 25rem;
            border: 2px rgb(226, 223, 223) solid;
            padding: 20px;
            border-radius: 10px;
        }

        .btn-custom {
            border: none
        }

        .btn-custom:hover {
            background-color: #0d6efd;
            border: none;
            color: white
        }

        .sign-in:hover {
            cursor: pointer;
            color: #0d6efd
        }

        .form-custom {
            display: block;
            width: 100%;
            border-radius: 6px;
            border: 1px #cfd4db solid;
            height: 2.5rem;
            padding: 2px 10px 2px
        }

        .form-custom:focus {
            border: 1px #0d6efd solid;
            outline: 1px #0d6efd solid;
        }

        #email:invalid {
            border: 2px red solid;
            outline: none !important
        }
    </style>
</head>

<body>
    <div class="login-container">
        <h4 class="fw-bold text-center text-white">Sign Up</h4>
        <div class="mt-5">
            <form action="#">
                <div class="mb-4">
                    <label for="email" class="form-label text-white">Email</label>
                    <input type="email" class="form-custom" id="email" placeholder="Enter Your Email">
                </div>
                <div class="mb-4">
                    <label for="password" class="form-label text-white">Password</label>
                    <input type="password" class="form-custom" id="password" placeholder="Create Your Password">
                </div>
                <div class="mb-4">
                    <label for="confirm_password" class="form-label text-white">Confirm Your Password</label>
                    <input type="confirm_password" class="form-custom" id="confirm_password"
                        placeholder="Confirm Your Password">
                </div>
                <div class="mt-5 text-center">
                    <button type="submit" class="btn btn-secondary btn-custom">
                        Sign Up
                    </button>
                    <div class="mt-1 text-center text-white">
                        <small>
                            Already registered? <u class="sign-in">Sign In</u>
                        </small>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
