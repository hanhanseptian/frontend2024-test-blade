<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body {
            height: 100vh;
            width: 100vw;
            font-family: 'Nunito', sans-serif;
            display: flex;
            align-items: center;
            justify-content: center;
            justify-items: center;
            background: linear-gradient(90deg, rgb(25, 21, 95) 0%, rgb(65, 65, 156) 35%, rgb(76, 215, 243) 100%);
        }

        .btn-custom {
            border: none;
            height: 2.5rem;
            border-radius: 5px;
            padding: 2px 20px 2px;
            font-size: 14px
        }

        .btn-custom:hover {
            background-color: #a6b1c2;
            cursor: pointer;
        }
    </style>
</head>

<body class="antialiased">
    <button class="btn-custom" onclick="window.location='{{ url('signup') }}'">
        Sign Up Now
    </button>
</body>

</html>
